<?php

namespace Bcfigueira\Backend\Database\Mysql;

use Illuminate\Support\ServiceProvider;

class DatabaseMysqlServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {       
        $this->publishes([__DIR__.'/migrations'=>\database_path('migrations/mysql/')]);
    }
}
